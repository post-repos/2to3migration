# How to migrate from Helm 2 to Helm 3

This is a test on migrating Helm 2 to Helm 3 on a scenario with a mix of Helm 2 + Tiller and Helm 2 Tiller-less releases.

_Post related to this repo at: [My Wordpress site](https://juanmatiasdelacamara.wordpress.com/?p=657)_
## Pre requisites

You already have on your path helm and helm3 binaries (for Helm 2 and Helm 3).

## The scenario

First, create a fake scenario to test the procedure.

### Create the K8s cluster

Use the option you want. I use K3s and [k3s-boot](https://juanmatiasdelacamara.wordpress.com/2020/02/26/missing-k3s-reset/) to start, stop and reset it.

### Create the namespaces

We will create two namespaces, one for the apps, one for the tiller-less helm's secrets.

```
kubectl create ns tiller-ns
kubectl create ns test-ns
```

### Deploy Tiller

I'm using K3s with Kubernetes version 1.17.2. If you have an old Helm 2 version you will have problems. Please, get last Helm 2 version (currently 2.16.3).

Since we will need tiller deployed the standard way, deploy it:

```
kubectl -n kube-system create serviceaccount tiller

kubectl create clusterrolebinding tiller \
  --clusterrole=cluster-admin \
  --serviceaccount=kube-system:tiller

helm init --service-account tiller
```

## Deploy fake app 1

cd into `fake-app1` and run:

```
helm install --name fake-app1 --namespace test-ns -f values.yaml .
```

Now if you run a `helm list` will get:

```
NAME     	REVISION	UPDATED                 	STATUS  	CHART          	APP VERSION	NAMESPACE
fake-app1	1       	Thu Feb 27 11:49:54 2020	DEPLOYED	fake-app1-0.1.0	1.0        	test-ns 
```

## Tiller-less

Using a new terminal emulator, run tiller so we can go tiller-less for the second app.

```
export TILLER_NAMESPACE=tiller-ns 
export HELM_HOST=localhost:44134  
/path/to/your/helm/helm2.16.3/linux-amd64/tiller  --storage=secret 2>/dev/null 1>/dev/null &
```

Just to check, if you ask for the `helm list` again you must get an empty string. (we have no apps on Helm 2 tiller-less)

## Deploy fake app 2

cd into `fake-app2` and run:

```
helm install --name fake-app2 --namespace test-ns -f values.yaml .
```

Now, the list should be:

```
NAME     	REVISION	UPDATED                 	STATUS  	CHART          APP VERSION	NAMESPACE
fake-app2	1       	Thu Feb 27 11:55:59 2020	DEPLOYED	fake-app2-0.1.01.0        	test-ns 
```

Now, if you go to the previous terminal (the one without tiller-less), the list should have only fake-app1.

## The migration

Ok, now we have a cluster, with two apps running in the same namespace, one deployed with Helm 2 + Tiller, the other with Helm 2 + Tiller-less. Let's start the migration.

### Helm 2to3

Previous to do the migration check your Helm 3 binary:

```
╰─ helm3 version
version.BuildInfo{Version:"v3.1.0", GitCommit:"b29d20baf09943e134c2fa5e1e1cab3bf93315fa", GitTreeState:"clean", GoVersion:"go1.13.7"}
```

Install the 2to3 plugin:

```
helm3 plugin install https://github.com/helm/helm-2to3
```

Check it:

```
╰─ helm3 plugin list                                     
NAME   	VERSION	DESCRIPTION                                                                  
2to3   	0.3.0  	migrate and cleanup Helm v2 configuration and releases in-place to Helm v3
```

### Move the config

The first step is to move the config:

```
helm3 2to3 move config
```

You will be asked for confirmation, if you were working with Helm 3 previously, its configuration maybe overwritten during this operation.

At this point, if you check your `helm list` in the two terminal emulators we have used, you still will see the apps deployed.

If you run a `helm3 plugin list` now, you should see the plugins you had for Helm 2 along _2to3_.

### Migrate Helm 2 + Tiller apps

First, let's go for the apps installed using Helm 2 + Tiller. You must specify each release, but it's easy the create a script that reads input from `helm list` and the run the following command for each item:

```
helm3 2to3 convert RELEASE-NAME --delete-v2-releases
```

_You can avoid the use of --delete-v2-releases and do a cleanup later, this option will migrate to Helm 3 and delete from Helm 2._

In our case, go to the first terminal (the one with Helm 2 + Tiller) and run:

```
helm3 2to3 convert fakse-app1 --delete-v2-releases
```

You should see:

```
╰─ helm3 2to3 convert fake-app1 --delete-v2-releases 

2020/02/27 12:18:17 Release "fake-app1" will be converted from Helm v2 to Helm v3.
2020/02/27 12:18:17 [Helm 3] Release "fake-app1" will be created.
2020/02/27 12:18:17 [Helm 3] ReleaseVersion "fake-app1.v1" will be created.
2020/02/27 12:18:17 [Helm 3] ReleaseVersion "fake-app1.v1" created.
2020/02/27 12:18:17 [Helm 3] Release "fake-app1" created.
2020/02/27 12:18:17 [Helm 2] Release "fake-app1" will be deleted.
2020/02/27 12:18:17 [Helm 2] ReleaseVersion "fake-app1.v1" will be deleted.
2020/02/27 12:18:17 [Helm 2] ReleaseVersion "fake-app1.v1" deleted.
2020/02/27 12:18:17 [Helm 2] Release "fake-app1" deleted.
2020/02/27 12:18:17 Release "fake-app1" was converted successfully from Helm v2 to Helm v3.
```

Now ask for the lists:

```
╰─ helm list

╰─ helm3 list -n test-ns
NAME     	NAMESPACE	REVISION	UPDATED                                	STATUS  CHART          	APP VERSION
fake-app1	test-ns  	1       	2020-02-27 14:49:54.878844615 +0000 UTC	deployedfake-app1-0.1.0	1.0 
```

Helm 2 list is empty. 

**Note**: on Helm 3 we need to specify the namespace since it stores the release information in the same namespace the release lives on.

Now, the important stuff... can we upgrade the app using Helm 3?  Let's try it.

Go to the `fake-app1` directory and modify the `replicaCount: 1` to `replicaCount: 2` in the _values.yaml_ file, and then:

```
helm3 upgrade fake-app1 -n test-ns -f values.yaml .
```

_Remember we need to specify the namespace._

It worked! Check your pods, you should have two of them for _fake-app1_.

As it was said, you can migrate all the apps one by one or creating a little script. 

I go for the one-by-one. This way, you can migrate one app, then modify its pipelines, check it works, then go for the next.

### Migrate Helm 2 + Tiller-less apps

The procedure here is similar to the one described before. Just need to add `--tiller-out-cluster` flag and run it on a terminal with access to the local Tiller.

So, let's do it.

Got to the _tiller-less_ terminal (the one for _fake-app2_). Check what you have for Helm 2 and Helm 3:

```
╰─ helm list 
NAME     	REVISION	UPDATED                 	STATUS  	CHART          APP VERSION	NAMESPACE
fake-app2	1       	Thu Feb 27 12:00:56 2020	DEPLOYED	fake-app2-0.1.01.0        	test-ns  

╰─ helm3 list -n test-ns
NAME     	NAMESPACE	REVISION	UPDATED                                	STATUS  CHART          	APP VERSION
fake-app1	test-ns  	3       	2020-02-27 12:29:04.368441147 -0300 -03	deployedfake-app1-0.1.0	1.0       
```

Ok, we have _fake-app2_ on Helm 2 + Tille-less and _fake-app1_ on Helm 3 (since they are running inside the same namespace).

Now, let's try it:

```
helm3 2to3 convert fake-app2 --tiller-out-cluster --delete-v2-releases --release-storage secrets --tiller-ns tiller-ns
```

_Note that since we didn't use a standard tiller namespace, and have set to use secrets instead of configmaps, we added two more flags to indicate these settings._

And then:

```
╰─ helm list 

╰─ helm3 list -n test-ns
NAME     	NAMESPACE	REVISION	UPDATED                                	STATUS  CHART          	APP VERSION
fake-app1	test-ns  	3       	2020-02-27 12:29:04.368441147 -0300 -03	deployedfake-app1-0.1.0	1.0        
fake-app2	test-ns  	1       	2020-02-27 15:00:56.260754274 +0000 UTC	deployedfake-app2-0.1.0	1.0 
```

Helm 2 + Tiller-less list an empty list, and both apps are now on Helm 3.

Let's try to upgrade the _fake-app2_, just to check. Modify the replicacount on the values.yaml file and run:

```
helm3 upgrade --install fake-app2 --namespace test-ns -f values.yaml --description "This is a custom description"
```

_Note I added a flag "--description"... this flag is here thanks to a change I introduced in Helm 3 and it's ready on Helm 3.1.0... I apologize by the intervention of my ego here._

### Clean up

Finally, clean up. Only when Helm 2 is not used any more run this (**it will delete all related Helm 2 stuff, not recoverable!**).

In the Helm 2 + Tiller terminal:

```
helm3 2to3 cleanup
```

_It should also remove tiller from the server._


In the Helm 2 + Tiller-less terminal:

```
helm3 2to3 cleanup --tiller-out-cluster --release-storage secrets --tiller-ns tiller-ns
```

## Conclusion

That's it, and Happy Helming and sailing with Helm 3 and your favorite TV serie with [Guilligan](https://www.imdb.com/title/tt0057751/).
